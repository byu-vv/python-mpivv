
# syntax
# send - s proc dst idx
# recv - r proc src idx
# wait - w proc idx
# barr - b proc group

# non-deterministic deadlock with message starvation

s 0 1 0
w 0 0

r 1 -1 0
w 1 0
r 1 0 2
w 1 2

s 2 1 0
w 2 0

