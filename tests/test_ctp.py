
import glob
import pytest

from mpivv import *
from tests.utils import *

ctps = glob.glob(os.path.join(fixture_dir, '*.ctp'))

@pytest.fixture(scope='session', params=ctps)
def ctp_source(request):
    return ctp.read(request.param)

def test_parse(ctp_source):
    prog = ctp.parse(ctp_source)
    lines = ctp.serialize(prog)
    assert lines == ctp_source

def test_parse_additions(ctp_source):
    prog = ctp.parse(ctp_source, add_extra=True)

    grps = set()
    for proc, actions in enumerate(prog):
        assert len(actions) > 2
        assert actions[-1].proc == proc
        assert actions[-1].is_bot
        assert actions[-2].proc == proc
        assert actions[-2].is_barr
        assert isinstance(actions[-2].grp, int)
        grps.add(actions[-2].grp)

    lines = ctp.serialize(prog)
    diff = set(lines) - set(ctp_source)
    barrs = [b for b in diff if b.startswith('b')]
    bots = [b for b in diff if b.startswith('e')]

    assert set(barrs) | set(bots) == diff

