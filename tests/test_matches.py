
from mpivv import *
from tests.utils import *

def test_1_1():
    prog = ctp.parse(ctp.read(fixture('1.ctp')))
    pairs = matches.over_approx(prog)
    expected = [
            (prog[1][0], prog[0][0]),
            (prog[1][0], prog[2][0]),
            (prog[1][2], prog[0][0]),
            ]

    assert len(pairs) == len(expected)
    assert set(pairs) == set(expected)

def test_diffusion2d4_over():
    prog = ctp.parse(ctp.read(fixture('diffusion2d4.ctp')))
    pairs = matches.over_approx(prog)
    expected = [
            (prog[0][0], prog[1][0]),
            (prog[0][0], prog[2][0]),
            (prog[0][0], prog[3][0]),

            (prog[0][2], prog[1][0]),
            (prog[0][2], prog[2][0]),
            (prog[0][2], prog[3][0]),
            (prog[0][2], prog[1][2]),
            (prog[0][2], prog[2][2]),
            (prog[0][2], prog[3][2]),

            (prog[0][4], prog[1][0]),
            (prog[0][4], prog[2][0]),
            (prog[0][4], prog[3][0]),
            (prog[0][4], prog[1][2]),
            (prog[0][4], prog[2][2]),
            (prog[0][4], prog[3][2]),
            (prog[0][4], prog[2][4]),
            (prog[0][4], prog[1][12]),

            (prog[0][6], prog[1][0]),
            (prog[0][6], prog[2][0]),
            (prog[0][6], prog[3][0]),
            (prog[0][6], prog[1][2]),
            (prog[0][6], prog[2][2]),
            (prog[0][6], prog[3][2]),
            (prog[0][6], prog[2][4]),
            (prog[0][6], prog[2][8]),
            (prog[0][6], prog[1][12]),
            (prog[0][6], prog[1][16]),

            (prog[0][8], prog[1][0]),
            (prog[0][8], prog[2][0]),
            (prog[0][8], prog[3][0]),
            (prog[0][8], prog[1][2]),
            (prog[0][8], prog[2][2]),
            (prog[0][8], prog[3][2]),
            (prog[0][8], prog[2][4]),
            (prog[0][8], prog[2][8]),
            (prog[0][8], prog[1][12]),
            (prog[0][8], prog[1][16]),

            (prog[0][10], prog[1][0]),
            (prog[0][10], prog[2][0]),
            (prog[0][10], prog[3][0]),
            (prog[0][10], prog[1][2]),
            (prog[0][10], prog[2][2]),
            (prog[0][10], prog[3][2]),
            (prog[0][10], prog[2][4]),
            (prog[0][10], prog[2][8]),
            (prog[0][10], prog[1][12]),
            (prog[0][10], prog[1][16]),

            (prog[0][14], prog[1][0]),
            (prog[0][14], prog[2][0]),
            (prog[0][14], prog[3][0]),
            (prog[0][14], prog[1][2]),
            (prog[0][14], prog[2][2]),
            (prog[0][14], prog[3][2]),
            (prog[0][14], prog[2][4]),
            (prog[0][14], prog[2][8]),
            (prog[0][14], prog[1][12]),
            (prog[0][14], prog[1][16]),

            (prog[0][18], prog[3][0]),
            (prog[0][18], prog[1][2]),
            (prog[0][18], prog[2][2]),
            (prog[0][18], prog[3][2]),
            (prog[0][18], prog[2][4]),
            (prog[0][18], prog[2][8]),
            (prog[0][18], prog[1][12]),
            (prog[0][18], prog[1][16]),

            (prog[0][22], prog[3][0]),
            (prog[0][22], prog[3][2]),
            (prog[0][22], prog[2][4]),
            (prog[0][22], prog[2][8]),
            (prog[0][22], prog[1][12]),
            (prog[0][22], prog[1][16]),

            (prog[0][26], prog[3][2]),
            (prog[0][26], prog[2][8]),
            (prog[0][26], prog[1][16]),

            (prog[1][6], prog[3][4]),
            (prog[1][6], prog[0][20]),

            (prog[1][10], prog[3][4]),
            (prog[1][10], prog[3][8]),
            (prog[1][10], prog[0][20]),
            (prog[1][10], prog[0][24]),

            (prog[1][14], prog[3][4]),
            (prog[1][14], prog[3][8]),
            (prog[1][14], prog[0][20]),
            (prog[1][14], prog[0][24]),

            (prog[1][18], prog[3][8]),
            (prog[1][18], prog[0][24]),

            (prog[2][6], prog[0][12]),
            (prog[2][6], prog[3][12]),

            (prog[2][10], prog[0][12]),
            (prog[2][10], prog[3][12]),
            (prog[2][10], prog[0][16]),
            (prog[2][10], prog[3][16]),

            (prog[2][14], prog[0][12]),
            (prog[2][14], prog[0][16]),
            (prog[2][14], prog[3][12]),
            (prog[2][14], prog[3][16]),

            (prog[2][18], prog[0][16]),
            (prog[2][18], prog[3][16]),

            (prog[3][6], prog[1][4]),
            (prog[3][6], prog[2][12]),

            (prog[3][10], prog[1][4]),
            (prog[3][10], prog[1][8]),
            (prog[3][10], prog[2][12]),
            (prog[3][10], prog[2][16]),

            (prog[3][14], prog[1][4]),
            (prog[3][14], prog[1][8]),
            (prog[3][14], prog[2][12]),
            (prog[3][14], prog[2][16]),

            (prog[3][18], prog[1][8]),
            (prog[3][18], prog[2][16]),
            ]

    assert len(pairs) == len(expected)
    assert set(pairs) == set(expected)

