
from mpivv import *
from tests.utils import *

def are_cyclic_perms(a, b):
    if not a:
        return not b
    idx = b.index(a[0])
    return a == b[idx:] + b[0:idx]

def test_1_1():
    prog = ctp.parse(ctp.read(fixture('1.ctp')))
    pairs = matches.over_approx(prog)
    vertices = sorted(utils.flatten_one_level(prog))
    edges = graph.edges(prog, pairs)
    cycles = list(johnson.cycles(vertices, edges))
    assert not cycles

def test_1_additions_1():
    prog = ctp.parse(ctp.read(fixture('1.ctp')), add_extra=True)
    pairs = matches.over_approx(prog)
    vertices = sorted(utils.flatten_one_level(prog))
    edges = graph.edges(prog, pairs)
    cycles = list(johnson.cycles(vertices, edges))
    expected = [prog[0][2], prog[0][3], prog[1][2], prog[1][3], prog[1][4]]
    assert cycles
    assert any(are_cyclic_perms(expected, cycle) for cycle in cycles)

