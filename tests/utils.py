
import glob
import os
import pytest

curdir = os.path.dirname(os.path.abspath(__file__))

fixture_dir = os.path.join(curdir, 'fixtures')

def fixture(name):
    return os.path.join(fixture_dir, name)

