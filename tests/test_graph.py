
from mpivv import *
from tests.utils import *

def assert_edges(prog, actual, expected):
    for action in utils.flatten_one_level(prog):
        assert action in actual, repr(action)
        assert action in expected, repr(action)
        assert len(actual[action]) == len(expected[action]), repr(action)
        assert set(actual[action]) == set(expected[action]), repr(action)

def test_1_1():
    prog = ctp.parse(ctp.read(fixture('1.ctp')))
    pairs = matches.over_approx(prog)
    edges = graph.edges(prog, pairs)
    expected = {
            prog[0][0]: [prog[0][1], prog[1][0], prog[1][2]],
            prog[0][1]: [],
            prog[1][0]: [prog[1][1], prog[0][0], prog[2][0]],
            prog[1][1]: [prog[1][2]],
            prog[1][2]: [prog[1][3], prog[0][0]],
            prog[1][3]: [],
            prog[2][0]: [prog[2][1], prog[1][0]],
            prog[2][1]: []
            }
    assert_edges(prog, edges, expected)

def test_1_additions_1():
    prog = ctp.parse(ctp.read(fixture('1.ctp')), add_extra=True)
    pairs = matches.over_approx(prog)
    edges = graph.edges(prog, pairs)
    expected = {
            prog[0][0]: [prog[0][1], prog[1][0], prog[1][2]],
            prog[0][1]: [prog[0][2]],
            prog[0][2]: [prog[0][3], prog[1][4]],
            prog[0][3]: [prog[1][2]],
            prog[1][0]: [prog[1][1], prog[0][0], prog[2][0]],
            prog[1][1]: [prog[1][2]],
            prog[1][2]: [prog[1][3], prog[0][0]],
            prog[1][3]: [prog[1][4]],
            prog[1][4]: [prog[1][5], prog[0][2]],
            prog[1][5]: [prog[2][0]],
            prog[2][0]: [prog[2][1], prog[1][0]],
            prog[2][1]: [prog[2][2]],
            prog[2][2]: [prog[2][3], prog[0][2], prog[1][4]],
            prog[2][3]: []
            }
    assert_edges(prog, edges, expected)

