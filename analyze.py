
import argparse
import logging
import os
from timeit import default_timer as timer
import sys

from mpivv import *

parser = argparse.ArgumentParser()
parser.add_argument('function', choices=['print', 'feasible', 'cycles', 'deadlocks', 'graph'])
parser.add_argument('filename', help='A CTP file (e.g. from tests/fixtures)')
parser.add_argument('--log', help='Log level integer or string from [DEBUG, INFO, WARNING, ERROR]', default='ERROR')
parser.add_argument('--extra', help='Add extra barrier and bottom actions to program', action='store_true', default=False)
parser.add_argument('--infinite', help='Use infinite buffer setting', action='store_true', default=False)
parser.add_argument('--cycle-limit', dest='cycle_limit', type=int, default=1000)
parser.add_argument('--deadlock-limit', dest='deadlock_limit', type=int, default=1)
parser.add_argument('--no-filter', dest='no_filter', action='store_true', default=False)
parser.add_argument('--no-prune', help='Find all cycles instead of cycles with deadlock shape only', dest='no_prune', action='store_true', default=False)
args = parser.parse_args()
assert args.extra if args.infinite else True

level = int(args.log) if args.log.isdigit() else getattr(logging, args.log, 'ERROR')
log_format = '%(levelname)s:%(filename)s:%(lineno)s:%(message)s'
logging.addLevelName(utils.JOHNSON_LOG_LEVEL, 'JOHNSON')
logging.addLevelName(utils.MACHINE_LOG_LEVEL, 'MACHINE')
logging.addLevelName(utils.SMT_LOG_LEVEL, 'SMT')
logging.addLevelName(utils.GRAPH_LOG_LEVEL, 'GRAPH')
logging.basicConfig(format=log_format, level=level)

prog = ctp.parse(ctp.read(args.filename), add_extra=args.extra, infinite=args.infinite)

if args.function == 'print':
    ctp.print_ascii(prog)

else:
    pairs = matches.over_approx(prog)
    vertices = sorted(utils.flatten_one_level(prog))
    match_order = graph.init_edges(prog)
    graph.add_match_order(prog, match_order)
    edges = graph.copy_edges(match_order)
    graph.add_matches(prog, pairs, edges)
    if level <= utils.GRAPH_LOG_LEVEL:
        for a in edges.keys():
            logging.log(utils.GRAPH_LOG_LEVEL, '%s => %s', a, edges[a])
    solver = smt.deadlock_solver(prog, match_order, pairs)

    if args.function == 'feasible':
        trace = solver.feasible_execution()
        if trace:
            ctp.print_ascii(trace)
        else:
            print('Not feasible')

    elif args.function in ('cycles', 'deadlocks'):
        num_cycles = 0
        num_filtered = 0
        num_deadlocks = 0
        num_unsat = 0
        start = timer()
        for cycle in johnson.cycles(vertices, edges, deadlock=(not args.no_prune)):
            num_cycles += 1
            if num_cycles >= args.cycle_limit:
                print('Reached cycle limit')
                break
            candidate = johnson.deadlock_candidate(cycle)
            if not args.no_filter:
                starved = any(a.is_bot for a in cycle)
                state = machine.execute(prog, candidate, starved=starved)
                if state.status == machine.status.UNREACHABLE:
                    logging.debug('Filtered candidate %s from cycle %s', candidate, cycle)
                    num_filtered += 1
                else:
                    logging.info('Candidate %s from cycle %s passed filter', candidate, cycle)
                    logging.info('Machine stopped here %s', state.ctrl)
                    if state.status == machine.status.REACHABLE:
                        logging.info('Machine found deadlock trace')
                        if level <= logging.DEBUG:
                            trace = solver.feasible_deadlock(state)
                            assert trace
                            ctp.print_ascii(trace, out=sys.stderr)
                        num_deadlocks += 1
                    elif args.function == 'deadlocks':
                        trace = solver.feasible_deadlock(state)
                        if trace:
                            logging.info('Found deadlock trace')
                            if level <= logging.DEBUG:
                                ctp.print_ascii(trace, out=sys.stderr)
                            num_deadlocks += 1
                            if num_deadlocks >= args.deadlock_limit:
                                print('Reached deadlock limit')
                                break
                        else:
                            num_unsat += 1

            else:
                logging.debug('Found candidate %s from cycle %s', candidate, cycle)
                if args.function == 'deadlocks':
                    trace = solver.feasible_deadlock(machine.default_candidate_state(prog, candidate))
                    if trace:
                        logging.info('Found deadlock trace')
                        if level <= logging.DEBUG:
                            ctp.print_ascii(trace, out=sys.stderr)
                        num_deadlocks += 1
                        if num_deadlocks >= args.deadlock_limit:
                            print('Reached deadlock limit')
                            break
                    else:
                        num_unsat += 1
        end = timer()
        if not args.no_filter:
            print('%s/%s cycles filtered' % (num_filtered, num_cycles))
        else:
            print('%s cycles' % (num_cycles))
        if args.function == 'deadlocks' or num_deadlocks > 0:
            print('%s deadlocks and %s unsat' % (num_deadlocks, num_unsat))
        print('finished in %s seconds' % (end - start))

    print('%s actions' % len(utils.flatten_one_level(prog.procs)))
    print('%s match pairs' % len(pairs))
    print('%s edges' % sum([len(edges[a]) for a in edges.keys()]))
