#!/bin/sh

cat | awk '
BEGIN {print "Fixture", "Setting", "Edges", "Filtered/Total", "Deadlocks", "Unsat", "Time" }
/Benchmarking/ { fixture = $5; setting = $7 }
/filtered/ { cycles_result = $1 }
/deadlocks/ { deadlocks_result = $1; unsat_result = $4 }
/finished/ { time = $3 }
/edges/ { size = $1; print fixture, setting, size, cycles_result, deadlocks_result, unsat_result, time }
' | column -t

