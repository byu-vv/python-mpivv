#!/bin/sh

if [[ ! -f analyze.py ]]
then
  echo Run this script from project root
  exit 1
fi

# zero buffer tests
for fixture in tests/fixtures/{diffusion2d4,diffusion2d8,is128,is256,is64,monte16,monte32,monte4,monte64,monte8}.ctp
do
  echo "Benchmarking filter in $fixture with zero-buffer setting"
  python analyze.py --cycle-limit 5000 --extra cycles $fixture
done

# infinite buffer tests
for fixture in tests/fixtures/{diffusion2d4,diffusion2d8,floyd16,floyd8,ge16,ge8,heat16,heat32,heat64,heat8,integrate10,integrate16,integrate8,is128,is256,is64,monte16,monte32,monte4,monte64,monte8}.ctp
do
  echo "Benchmarking filter in $fixture with infinite-buffer setting"
  python analyze.py --cycle-limit 5000 --infinite --extra cycles $fixture
done

