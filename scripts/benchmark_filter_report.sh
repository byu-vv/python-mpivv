#!/bin/sh

cat | awk '
BEGIN {print "Fixture", "Setting", "Edges", "Filtered/Total", "Time" }
/Benchmarking/ { fixture = $4; setting = $6 }
/filtered/ { result = $1 }
/finished/ { time = $3 }
/edges/ { size = $1; print fixture, setting, size, result, time }
' | column -t

