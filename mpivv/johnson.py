"""Detect deadlock cycles in a graph with modified Johnson's algorithm"""

from mpivv.utils import *

def induce_subgraph(V, E):
    """
    :param V: an ordered list of CTP actions to generate sub graph with
    :param E: adjacency lists for actions in V
    :returns: the edges of subgraph (V, E') where E' is induced by V
    """
    vset = set(V)
    edges = {}
    for act in E.keys():
        edges[act] = [a for a in E[act] if a in vset]
    return edges

def least_tarjan_scc(V, E):
    """
    A simple adaptation of the strong component algorithm presented in
    "Depth-First Search and Linear Graph Algorithms" by Robert Tarjan

    :param V: an ordered list of CTP actions
    :param E: adjacency lists for actions in V
    :returns: adjacency lists of strongly connected component with least vertex in graph (V, E) along with the least vertex itself in a tuple
    """
    i = 0
    stack = []
    lowlink = {}
    number = {}

    least = None
    least_scc = None

    def scc(v):
        nonlocal i, least, least_scc
        lowlink[v] = number[v] = i = i + 1
        stack.append(v)

        for w in E[v]:
            if w not in number:
                scc(w)
                lowlink[v] = min([lowlink[v], lowlink[w]])
            elif number[w] < number[v]:
                if w in stack:
                    lowlink[v] = min([lowlink[v], number[w]])

        if lowlink[v] == number[v]:
            component = set()
            newleast = False
            while stack and number[stack[-1]] >= number[v]:
                w = stack.pop()
                if least is None or w < least:
                    least = w
                    newleast = True
                component.add(w)
            if newleast:
                least_scc = component.copy()

    for v in V:
        if v not in number:
            scc(v)

    if least is None:
        return (None, {})

    A = {}
    for w in least_scc:
        A[w] = [a for a in E[w] if a in least_scc]
    return (least, A)

def cycles(V, E, deadlock=True):
    """
    A simple adaptation of the algorithm presented in
    "Finding All the Elementary Circuits of a Directed Graph" by Donald B. Johnson

    :param V: an ordered list of CTP actions
    :param E: adjacency lists for actions in V
    :param deadlock: don't pursue cycles that do not conform to deadlock shape
    :returns: a generator for deadlock cycles in graph (V, E)
    """
    stack = []
    blocked = {}
    blists = {}

    block_count = {}
    orphaned = []
    orphaned_paths = []
    proc_stack = []

    def good_edge(v, w, s):
        """
        This function disregards an edge (v, w) if w cannot possibly extend the current stack to create a deadlock cycle.
        A deadlock cycle C must obey these rules.
        - C must visit a blocking action in every process that it visits.
          The earliest such blocking action in each process is named the deadlocked action for that process.
        - C must contain only one outgoing edge from each process and that the source action of that edge must be process ordered after the deadlocked action of that process.
        - Let orphaned be all send and receive actions visited by C that are process ordered before a deadlocked action also visited by C.
          Then for every pair of actions (a, b) in orphaned, a cannot possibly match b
        This function rejects an edge if it will cause the current stack to violate any of these rules.
        It also rejects an edge if it will produce a candidate that was already produced earlier.
        A few data structures are used
        - block_count: proc -> int, keeps track of the number of blocking actions visited by stack in each process
        - orphaned: set(action), actions orphaned by the potential deadlock represented by stack
        - orphaned_paths: set(tuple(proc, act)), keeps track of previous orphaned actions to prevent duplicate candidates
        - proc_stack: procs in order visited by stack
        """
        if v.proc == w.proc:
            return True
        # v.proc != w.proc
        if v.proc not in block_count or not block_count[v.proc]:
            # we have not visited the deadlocked action of v.proc yet
            return False
        if v.is_barr and block_count[v.proc] == 1:
            # v is the deadlocked action, need to travel down v.proc before visiting another proc
            return False
        if w.is_recv and w.src == -1:
            # wildcard recvs can't be orphaned non-deterministically... right?
            return False
        if any(can_match(a, w) for a in orphaned):
            # w can match another orphaned action
            return False
        if (v.proc, w) in orphaned_paths:
            # w was orphaned by v.proc in a previous cycle
            return False
        if w is not s:
            # w is not in the starting process
            if any(a.proc == w.proc for a in stack):
                # we have visited w.proc before, we can't visit it again in a simple deadlock cycle
                return False
            return True
        return True

    def unblock(v):
        blocked[v] = False
        while len(blists[v]):
            w = blists[v].pop(0)
            if blocked[w]:
                unblock(w)

    def circuit(A, s, v):
        nonlocal orphaned_paths
        found = False
        if not stack or stack[-1].proc != v.proc:
            proc_stack.append(v.proc)
            orphaned.append(v)
        stack.append(v)
        blocked[v] = True

        if v.is_wait or v.is_barr:
            block_count[v.proc] += 1

        for w in A[v]:
            if deadlock and not good_edge(v, w, s):
                continue
            if v.proc != w.proc:
                orphaned_paths.append((v.proc, w))
            if w == s:
                yield stack[:]
                found = True
            elif not blocked[w]:
                found = yield from circuit(A, s, w)

        if found:
            unblock(v)
        else:
            for w in A[v]:
                if v not in blists[w]:
                    blists[w].append(v)

        if orphaned[-1] is stack[-1]:
            orphaned.pop()
            proc_stack.pop()
            orphaned_paths = [(p, a) for p, a in orphaned_paths if p != stack[-1].proc]
        stack.pop()

        if v.is_wait or v.is_barr:
            block_count[v.proc] -= 1

        return found

    i = 0
    while i < len(V):
        V_i = V[i:]
        E_i = induce_subgraph(V_i, E)
        s, A = least_tarjan_scc(V_i, E_i)
        if s is not None:
            i += V_i.index(s)
            stack.clear()
            blists.clear()
            block_count.clear()
            for w in A.keys():
                blocked[w] = False
                blists[w] = []
                block_count[w.proc] = 0
            orphaned.clear()
            orphaned_paths.clear()
            proc_stack.clear()
            yield from circuit(A, s, s)
            i += 1
        else:
            i = len(V)

def deadlock_candidate(cycle):
    """
    :param cycle: a list of actions creating a cycle
    :returns: a list with only the first blocking actions in cycle for each process
    """
    blocking = [a for a in cycle if a.is_barr or a.is_wait]
    return [a for a in blocking if all(b.idx >= a.idx for b in blocking if b.proc == a.proc)]

