"""Encode deadlock candidates as SMT formulae and test for feasibility"""

from collections import namedtuple
import logging
import z3

from mpivv.utils import *
import mpivv.machine as machine

send_vars = namedtuple('send_vars', ['t', 'm', 'w', 'c']) # sends store their timestamp, the timestamps for their match and wait and whether they complete
recv_vars = namedtuple('recv_vars', ['t', 'm', 'w', 'c']) # recvs store their timestamp, the timestamps for their match and wait and whether they complete
wait_vars = namedtuple('wait_vars', ['t', 'c']) # waits store their timestamp and whether they complete
barr_vars = namedtuple('barr_vars', ['t', 'c']) # barrs store their timestamp and whether they complete

def mkvars(prog):
    """
    :param prog: a CTP
    :returns: Z3 variables for actions in CTP
    """
    result = {}
    mksend = lambda a: send_vars(z3.Int('time' + str(a)), z3.Int('match' + str(a)), z3.Int('wait' + str(a)), z3.Bool('complete' + str(a)))
    mkrecv = lambda a: recv_vars(z3.Int('time' + str(a)), z3.Int('match' + str(a)), z3.Int('wait' + str(a)), z3.Bool('complete' + str(a)))
    mkwait = lambda a: wait_vars(z3.Int('time' + str(a)), z3.Bool('complete' + str(a)))
    mkbarr = lambda a: barr_vars(z3.Int('time' + str(a)), z3.Bool('complete' + str(a)))
    for proc in prog:
        for act in proc:
            if act.is_send:
                result[act] = mksend(act)
            elif act.is_recv:
                result[act] = mkrecv(act)
            elif act.is_wait:
                result[act] = mkwait(act)
            elif act.is_barr:
                result[act] = mkbarr(act)
    return result

def pairs_to_table(pairs):
    """
    :param pairs: a set of match pairs for the actions in prog
    :returns: a dict {act: [matches for act]}
    """
    table = {}
    for fst, snd in pairs:
        if fst not in table:
            table[fst] = []
        if snd not in table:
            table[snd] = []
        table[fst].append(snd)
        table[snd].append(fst)
    return table

class deadlock_solver:
    """
    Tests a deadlock candidate for feasibility using the encoding described in
    "A Predictive Analysis for Deadlock Detection in MPI Programs with Pattern Matching" by Huang et al.
    """

    def __init__(self, prog, edges, pairs):
        """
        :param prog: a CTP as a two dimensional array of actions
        :param edges: adjacency lists for actions in prog representing match order dependencies
        :param pairs: a set of match pairs for the actions in prog
        """
        self.prog = prog
        self.edges = edges
        self.matches = pairs_to_table(pairs)
        self.solver = z3.Solver()
        self.vars = mkvars(prog)

    def match(self, recv, send):
        return z3.And(
                self.vars[recv].m == self.vars[send].t, # recv stores send's timestamp
                self.vars[send].m == self.vars[recv].t, # send stores recv's timestamp
                self.vars[send].t < self.vars[recv].w, # send happens before recv's wait
                self.vars[recv].t < self.vars[send].w, # wait happens before send's wait (this works because we remove waits on sends for infinite buffer),
                self.vars[recv].c, # this recv is complete
                self.vars[send].c # this send is complete
                )

    def check(self, state):
        """
        :param state: final state of abstract machine
        :returns: SAT model or None
        """
        deadlock_procs = set([a.proc for a in state.candidate]) if state.candidate else list(range(len(self.prog)))
        deadlock_reqs = set([a.req for a in state.candidate if a.is_wait])
        acts = [a for a in flatten_one_level(self.prog.procs) if a.idx < state.ctrl[a.proc] and not a.is_bot]
        times = [self.vars[a].t for a in acts if a.is_send or a.is_recv or a.is_wait]
        groups = {}

        # constraint definitions
        completes_before = lambda a, b: z3.And(z3.Implies(self.vars[b].c, self.vars[a].c), self.vars[a].t < self.vars[b].t)
        must_complete = lambda act: self.vars[act].c == True
        recv_match = lambda recv: z3.Or(*[self.match(recv, send) for send in self.matches[recv] if send not in deadlock_reqs and send.idx < state.ctrl[send.proc]])
        send_match = lambda send: z3.Or(*[self.match(recv, send) for recv in self.matches[send] if recv not in deadlock_reqs and recv.idx < state.ctrl[recv.proc]])
        match_if_complete = lambda act: z3.Implies(self.vars[act].c, send_match(act) if act.is_send else recv_match(act))
        fifo_non_overtaking = lambda a, b: self.vars[a].m < self.vars[b].m
        nearest_wait = lambda a, w: self.vars[a].w == self.vars[w].t
        barr_group = lambda a, b: self.vars[a].t == self.vars[b].t
        unique_times = lambda: z3.Distinct(*times)
        unique_matches = lambda: z3.Distinct(*[self.vars[a].m for a in acts if a.is_send or a.is_recv])

        for act in acts:
            for succ in self.edges[act]:
                if succ.idx < state.ctrl[succ.proc] and not succ.is_bot:
                    self.solver.add(completes_before(act, succ))
            if act.is_send or act.is_recv:
                self.solver.add(match_if_complete(act))
                if act.wait and act.wait.idx < state.ctrl[act.proc]:
                    self.solver.add(nearest_wait(act, act.wait))
                if act.rank > 0:
                    msgqs = self.prog.sendqs if act.is_send else self.prog.recvqs
                    pred = msgqs[(act.src, act.dst)][act.rank - 1]
                    self.solver.add(fifo_non_overtaking(pred, act))
            elif act.is_wait:
                if act.proc in deadlock_procs:
                    self.solver.add(must_complete(act))
            elif act.is_barr:
                self.solver.add(must_complete(act))
                if act.grp not in groups:
                    groups[act.grp] = act
                    times.append(self.vars[act].t)
                else:
                    self.solver.add(barr_group(groups[act.grp], act))
        for act in state.candidate:
            if act.is_wait:
                for match in self.matches[act.req]:
                    self.solver.add(must_complete(match))
        self.solver.add(unique_times())
        self.solver.add(unique_matches())

        result = self.solver.check()
        return self.solver.model() if result == z3.sat else None

    def feasible_execution(self):
        """
        :returns: an execution that reaches the end of the program or None
        """
        status = machine.status.UNKNOWN
        ctrl = [len(p) for p in self.prog]
        candidate = [] # special candidate meaning end of program
        return self.feasible_deadlock(machine.state(candidate, status, ctrl))

    def feasible_deadlock(self, state):
        """
        :param state: final state of abstract machine
        :returns: an execution that reaches the deadlock or None
        """
        self.solver.push()
        model = self.check(state)
        self.solver.pop()
        return self.trace(state, model) if model else []

    def trace(self, state, model):
        """
        :param state: final state of abstract machine
        :param model: SAT model of deadlock state
        :returns: a 2 dimensional array of strings that can be printed with ctp.print_ascii showing the deadlock trace
        """
        acts = [a for a in flatten_one_level(self.prog.procs) if a.idx < state.ctrl[a.proc] and not a.is_bot]
        acts.sort(key=lambda a: model.eval(self.vars[a].t).as_long())
        time_to_act = {model.eval(self.vars[a].t).as_long(): a for a in acts}
        trace = [[] for _ in range(len(self.prog))]
        def add_step(proc, s):
            for p in range(len(self.prog)):
                trace[p].append(s if p == proc else '')
        for act in acts:
            s = str(act)
            if act.is_send or act.is_recv:
                match_time = model.eval(self.vars[act].m).as_long()
                if match_time in time_to_act and can_match(act, time_to_act[match_time]):
                    s += 'x' + str(time_to_act[match_time])
            add_step(act.proc, s)
        for act in state.candidate:
            add_step(act.proc, str(act))
            add_step(act.proc, '-' * len(str(act)))
        return trace
