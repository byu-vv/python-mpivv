"""Functions to build dependency graph"""

from mpivv.utils import *

def init_edges(prog):
    """
    :param prog: a CTP as a two dimensional array of actions
    :returns: adjacency lists for actions in prog
    """
    return {a: [] for a in flatten_one_level(prog)}

def copy_edges(edges):
    """
    :param edges: adjacency lists for actions in prog
    :returns: a copy of edges to mutate
    """
    return {a: edges[a][:] for a in edges.keys()}

def add_match_order(prog, edges):
    """
    Add match order dependencies to edges

    :param prog: a CTP as a two dimensional array of actions
    :param edges: adjacency lists for actions in prog
    :param returns: adjacency lists for actions in prog representing communication dependencies due to MPI semantics
    """
    nprocs = len(prog)
    sendqs = message_queues(nprocs)
    recvqs = message_queues(nprocs)
    bottoms = {}

    def connected(src, snk): # called before cycles are formed so no infinite loops
        for act in edges[src]:
            if act is snk:
                return True
            return connected(act, snk)
        return False

    def connect(src, snk):
        if not connected(src, snk):
            edges[src].append(snk)

    def connect_blocking(src):
        trans = set()
        for succ in prog[src.proc][src.idx+1:]:
            if succ not in trans and not succ.is_bot: # transitive or added later
                edges[src].append(succ)
            trans.update(edges[succ])
            if succ.is_wait or succ.is_barr:
                break

    for proc in prog:
        for act in proc[::-1]: # connect blocking to successors and requests
            if act.is_wait:
                req = act.req
                edges[req].append(act)
                connect_blocking(act)
            elif act.is_barr:
                connect_blocking(act)
            elif act.is_bot:
                bottoms[act.proc] = act

        for act in proc: # connect requests on same endpoint and connect actions to bottom
            if act.is_send:
                endpoint = (act.src, act.dst)
                if sendqs[endpoint]:
                    connect(sendqs[endpoint][-1], act)
                sendqs[endpoint] = [act]
            elif act.is_recv:
                endpoint = (act.src, act.dst)
                wildcard = (-1, act.dst)
                if recvqs[endpoint]:
                    connect(recvqs[endpoint][-1], act)
                if wildcard != endpoint and recvqs[wildcard]:
                    connect(recvqs[wildcard][-1], act)
                recvqs[endpoint] = [act]

            if not edges[act] and act.proc in bottoms and act != bottoms[act.proc]:
                edges[act].append(bottoms[act.proc])

    return edges

def add_matches(prog, pairs, edges):
    """
    :param prog: a CTP as a two dimensional array of actions
    :param pairs: a list of match pairs from the actions in prog
    :param edges: adjacency lists for actions in prog
    :returns: adjacency lists for actions in prog representing potential communication dependencies due to matches
    """
    sendqs = prog.sendqs.copy()
    recvqs = prog.recvqs.copy()
    nprocs = len(prog)
    for proc in prog.bots.keys():
        wildcard = (-1, proc)
        if wildcard in recvqs and recvqs[wildcard]: # connect bottom action to requests that can be starved
            wildcardq = recvqs[wildcard][:]
            for p in range(nprocs):
                endpoint = (p, proc)
                nrecvs = 0
                if endpoint in recvqs:
                    for act in recvqs[endpoint]:
                        nrecvs += 1
                        if wildcardq and act.idx < wildcardq[0].idx:
                            continue
                        if act.src not in prog.bots:
                            continue
                        while wildcardq and wildcardq[0].idx < act.idx:
                            nrecvs += 1
                            wildcardq.pop(0)
                        if endpoint in sendqs and nrecvs <= len(sendqs[endpoint]):
                            continue
                        edges[prog.bots[act.src]].append(act)
                if endpoint in sendqs:
                    for rank, act in enumerate(sendqs[endpoint]):
                        if endpoint in recvqs and rank <= len(recvqs[endpoint]):
                            continue
                        edges[prog.bots[proc]].append(act)

    for recv, send in pairs:
        edges[recv].append(send)
        edges[send].append(recv)

    barrs = set()
    def can_reach_outgoing(act):
        for snk in edges[act]:
            if snk.proc != act.proc:
                return True
        for snk in edges[act]:
            if can_reach_outgoing(snk):
                return True
        return False
    # add barrier edges last to prune final barrier edges when possible
    for grp in prog.groups.keys():
        barrs.clear()
        for barr in prog.groups[grp]:
            if not barr.is_extra or can_reach_outgoing(barr):
                # only add in-coming edges to a final barrier if it can reach another process downstream
                barrs.add(barr)
        for src in prog.groups[grp]:
            for snk in barrs:
                if src is not snk:
                    edges[src].append(snk)

    return edges

def edges(prog, pairs):
    """
    :param prog: a CTP as a two dimensional array of actions
    :param pairs: a list of match pairs from the actions in prog
    :returns: entire dependency graph with match order and matches
    """
    edges = init_edges(prog)
    add_match_order(prog, edges)
    add_matches(prog, pairs, edges)
    return edges

