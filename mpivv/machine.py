"""
Implements abstract machine for filtering deadlock candidates
"""

import itertools
import logging
import enum

from mpivv.utils import *

class status(enum.Enum):
    """Status of deadlock candidate"""
    UNREACHABLE = 0
    UNKNOWN = 1
    REACHABLE = 2

class state:
    """Abstract machine state"""
    def __init__(self, candidate, status, ctrl):
        self.candidate = candidate
        self.status = status
        self.ctrl = ctrl

def default_candidate_state(prog, candidate):
    """
    :param prog: a CTP as a two dimensional array of actions
    :param candidate: a partial control point as a list of actions
    :returns: a conservative guess for the reachable deadlock state
    """
    nprocs = len(prog)
    ctrl = [len(prog[p]) for p in range(nprocs)]
    send_counts = {ep: len(prog.sendqs(ep)) for ep in endpoints(nprocs) if ep in prog.sendqs}
    recv_counts = {ep: len(prog.recvqs(ep)) for ep in endpoints(nprocs) if ep in prog.recvqs}
    for act in candidate:
        ctrl[act.proc] = act.idx
        for proc in range(nprocs):
            recv_ep = (proc, act.proc)
            if recv_ep in prog.recvqs:
                recv_counts[recv_ep] = len([a for a in prog.recvqs[recv_ep] if a.idx < act.idx])
            send_ep = (act.proc, proc)
            if send_ep in prog.sendqs:
                send_counts[send_ep] = len([a for a in prog.sendqs[send_ep] if a.idx < act.idx])
    return state(candidate, status.UNKNOWN, ctrl, send_counts, recv_counts)

def execute(prog, candidate, starved=False):
    """
    Executes program to specified control point

    :param prog: a CTP as a two dimensional array of actions
    :param candidate: a partial control point as a list of actions
    :param starved: whether any action in candidate was starved in cycle
    :returns: (status, list) tuple indicating where the abstract machine stopped and whether the control point is reachable
    """

    nprocs = len(prog)
    sendqs = message_queues(nprocs)
    recvqs = message_queues(nprocs)
    send_counts = {ep: 0 for ep in endpoints(nprocs)}
    recv_counts = {ep: 0 for ep in endpoints(nprocs)}

    ctrl = list(itertools.repeat(0, nprocs))
    pt = list(itertools.repeat(-1, nprocs))
    for act in candidate:
        pt[act.proc] = act.idx
    
    def debug_state(msg):
        logging.log(MACHINE_LOG_LEVEL, msg)
        logging.log(MACHINE_LOG_LEVEL, 'ctrl %s', ctrl)
        logging.log(MACHINE_LOG_LEVEL, 'pt %s', pt)
        logging.log(MACHINE_LOG_LEVEL, 'sendqs %s', sendqs)
        logging.log(MACHINE_LOG_LEVEL, 'recvqs %s', recvqs)

    def consume(inq, outq, num):
        for _ in range(num):
            inq.pop(0)
            outq.pop(0)

    def retvals():
        reached = all(pt[p] in (ctrl[p], -1) for p in range(nprocs))
        if not reached:
            return state(candidate, status.UNREACHABLE, ctrl)
        else:
            return state(candidate, status.UNKNOWN, ctrl)
        # elif starved or not prog.infinite:
        #     return state(candidate, status.UNKNOWN, ctrl)
        # elif all(send_counts[(w.req.src, w.req.dst)] < recv_counts[(w.req.src, w.req.dst)] for w in candidate if w.is_wait):
        #     return state(candidate, status.REACHABLE, ctrl)
        # else:
        #     return state(candidate, status.UNREACHABLE, ctrl)

    while True:

        # copy old state
        old_ctrl = ctrl[:]

        # check if control point was reached
        if all(pt[p] in (ctrl[p], -1) for p in range(nprocs)):
            debug_state("control point reached")
            return retvals()

        # round robin procs
        for proc in range(nprocs):
            # consume actions until we can't
            while True:

                # skip processes that are done
                if ctrl[proc] == len(prog[proc]):
                    debug_state("reached end of process %s" % proc)
                    break
                act = prog[proc][ctrl[proc]]
                debug_state("stepping proc %s with %s" % (proc, act))

                # park proc at control point
                if pt[proc] == ctrl[proc]:
                    debug_state("parking proc %s with %s" % (proc, act))
                    assert act.is_wait or act.is_barr
                    break

                # process action
                if act.is_send:
                    # update queues and advance
                    sendqs[(act.src, act.dst)].append(act)
                    send_counts[(act.src, act.dst)] += 1
                    sendqs[(-1, act.dst)].append(act)
                    send_counts[(-1, act.dst)] += 1
                    ctrl[proc] += 1

                elif act.is_recv:
                    # update queue and advance
                    recvqs[(act.src, act.dst)].append(act)
                    recv_counts[(act.src, act.dst)] += 1
                    ctrl[proc] += 1

                elif act.is_wait:
                    # get last communication action and message queues
                    req = act.req
                    assert req.is_send or req.is_recv
                    if req.is_recv:
                        outq = recvqs[(req.src, req.dst)]
                        inq = sendqs[(req.src, req.dst)]

                        # if req hasn't already been matched
                        if req in outq:
                            idx = outq.index(req)
                            # context switch if matching action hasn't been issued
                            if len(inq) <= idx:
                                debug_state('matching action for %s not issued' % req)
                                break
                            debug_state('consuming matches for %s' % req)
                            consume(inq, outq, idx + 1)

                    else:
                        outq = sendqs[(req.src, req.dst)]
                        woutq = sendqs[(-1, req.dst)]
                        inq = recvqs[(req.src, req.dst)]
                        winq = recvqs[(-1, req.dst)]

                        # if req hasn't already been matched
                        if req in outq and req in woutq:
                            idx = outq.index(req)
                            widx = woutq.index(req)
                            # context switch if matching action hasn't been issued
                            if len(inq) <= idx and len(winq) <= widx:
                                debug_state('matching action for %s not issued' % req)
                                break
                            elif len(inq) > idx:
                                debug_state('consuming matches for %s' % req)
                                consume(inq, outq, idx + 1)
                            else:
                                debug_state('consuming wildcard matches for %s' % req)
                                consume(winq, woutq, widx + 1)

                    ctrl[proc] += 1

                elif act.is_barr:
                    # advance all barriers at once, otherwise context switch
                    barrs = [prog[p][ctrl[p]] for p in range(nprocs)]
                    if not all(b.is_barr for b in barrs):
                        break
                    for p in range(nprocs):
                        # keep parked procs at the same point
                        if ctrl[p] not in (len(prog[p]), pt[p]):
                            ctrl[p] += 1
                    # TODO support barrier groups
                elif act.is_bot:
                    ctrl[proc] += 1

                debug_state("stepped proc %s with %s" % (proc, act))

            # check for deadlock
            if all(old_ctrl[p] == ctrl[p] for p in range(nprocs)):
                debug_state("deadlock or reached control point")
                return retvals()

