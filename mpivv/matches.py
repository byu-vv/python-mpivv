"""Over approximate potential match pairs in CTP"""

from mpivv.utils import *

def over_approx(prog):
    """
    Over approximate possible match pairs

    :param prog: a CTP as a two dimensional array of actions
    :returns: an over approximated set of match pairs
    """
    nprocs = len(prog)
    pairs = []
    for proc in range(nprocs):
        sends = {p: [a for a in prog[p] if a.is_send and a.dst == proc] for p in range(nprocs)}
        psends = lambda p: len(sends[p])
        stotal = sum(map(psends, sends.keys()))
        rcounts = {ep: 0 for ep in endpoints(nprocs)}
        for recv in [a for a in prog[proc] if a.is_recv]:
            srcs = [recv.src] if recv.src != -1 else [p for p in range(nprocs) if p != recv.proc]
            wrank = rcounts[(-1, recv.dst)]
            for src in srcs:
                rrank = rcounts[(src, recv.dst)]
                lower_bound = rrank + max(0, wrank - (stotal - psends(src)))
                upper_bound = rrank + wrank + 1
                pairs.extend([(recv, send) for send in sends[src][lower_bound:upper_bound]])
            rcounts[(recv.src, recv.dst)] += 1
    return pairs

