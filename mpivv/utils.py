
import itertools
import logging
import sys

JOHNSON_LOG_LEVEL = 1
MACHINE_LOG_LEVEL = 2
SMT_LOG_LEVEL = 3
GRAPH_LOG_LEVEL = 4

def can_match(v, w):
    """
    :param v: a CTP action
    :param w: a CTP action
    :returns: whether v and w form a type compatible match
    """
    if v.is_send:
        return w.is_recv and w.dst == v.dst and w.src in (-1, v.src)
    elif v.is_recv:
        return w.is_send and w.dst == v.dst and v.src in (-1, w.src)
    return False

def endpoints(nprocs):
    """
    :param nprocs: number of processes in CTP
    :returns: a list of endpoints
    """
    return [(-1, p) for p in range(nprocs)] + list(itertools.permutations(range(nprocs), 2))

def message_queues(nprocs):
    """
    :param nprocs: number of processes in CTP
    :returns: a dict of messages queues for each endpoint
    """
    return {ep: [] for ep in endpoints(nprocs)}

flatten_one_level = lambda nested: list(itertools.chain.from_iterable(nested))

