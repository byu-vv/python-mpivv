
import re
import sys

from mpivv.utils import *

class action:
    """CTP action"""

    def __init__(self, ident, proc, idx, rank=None, src=None, dst=None, req=None, grp=None, extra=False):
        self.ident = ident # index in execution
        self.idx = idx # index in process
        self.rank = rank # index in queue
        self.proc = proc
        self.src = src
        self.dst = dst
        self.req = req
        self.grp = grp
        self.is_send = proc == src and src is not None
        self.is_recv = proc == dst and dst is not None
        self.is_wait = req is not None
        self.is_barr = grp is not None
        self.is_extra = extra # whether this action was added to the program by the analysis
        types = len([b for b in (self.is_send, self.is_recv, self.is_wait, self.is_barr) if b])
        assert types in (0, 1)
        self.is_bot = types == 0
        self.wait = None

    def __repr__(self):
        if self.is_send:
            return "(s " + str(self.proc) + " " + str(self.idx) + " " + str(self.src) + " " + str(self.dst) + ")"
        elif self.is_recv:
            return "(r " + str(self.proc) + " " + str(self.idx) + " " + str(self.src) + " " + str(self.dst) + ")"
        elif self.is_wait:
            return "(w " + str(self.proc) + " " + str(self.idx) + " " + str(self.req.idx) + ")"
        elif self.is_barr:
            return "(b " + str(self.proc) + " " + str(self.idx) + " " + str(self.grp) + ")"
        elif self.is_bot:
            return "(e " + str(self.proc) + " " + str(self.idx) + ")"
        return "(u " + str(self.proc) + " " + str(self.idx) + ")"

    def __str__(self):
        return repr(self)

    def __eq__(self, other):
        return self.ident == other.ident and \
                self.proc == other.proc and \
                self.idx == other.idx and \
                self.src == other.src and \
                self.dst == other.dst and \
                self.req == other.req and \
                self.grp == other.grp

    def __hash__(self):
        return self.proc * 31 + self.idx * 7

    def __lt__(self, other):
        return self.ident < other.ident

    def __le__(self, other):
        return self.ident < other.ident or self == other

class CTP:
    """Simple container class"""

    def __init__(self, infinite):
        self.procs = []
        self.groups = {}
        self.sendqs = {}
        self.recvqs = {}
        self.bots = {}
        self.infinite = infinite
        self.filtered = {'total': 0}
        self.maxarg = -100

    def __len__(self):
        return len(self.procs)

    def __iter__(self):
        return iter(self.procs)

    def __getitem__(self, proc):
        return self.procs[proc]

    def handle_proc(self, proc):
        while proc > len(self.procs) - 1:
            self.procs.insert(proc, [])
        if proc not in self.filtered:
            self.filtered[proc] = 0

    def append(self, act):
        if act.is_barr:
            if act.grp not in self.groups:
                self.groups[act.grp] = []
            self.groups[act.grp].append(act)
        elif act.is_bot:
            assert not any(a.is_bot for a in self.procs[act.proc])
            self.bots[act.proc] = act
        self.procs[act.proc].append(act)

    def add_extra(self):
        maxident = len(flatten_one_level(self.procs)) - 1
        grp = self.maxarg + 1
        self.groups[grp] = []
        for proc in range(len(self)):
            maxident += 1
            maxidx = len(self[proc])
            barr = action(maxident, proc, maxidx, grp=self.maxarg, extra=True)
            self[proc].append(barr)
            self.groups[grp].append(barr)
            maxident += 1
            maxidx = len(self[proc])
            assert not any(a.is_bot for a in self[proc])
            bot = action(maxident, proc, maxidx, extra=True)
            self[proc].append(bot)
            self.bots[proc] = bot

    def append(self, ident, typ, proc, arg):
        self.handle_proc(proc)
        ident -= self.filtered['total']
        idx = len(self[proc])
        if arg > self.maxarg:
            self.maxarg = arg
        if typ == 's':
            ep = (proc, arg)
            if ep not in self.sendqs:
                self.sendqs[ep] = []
            act = action(ident, proc, idx, rank=len(self.sendqs[ep]), src=proc, dst=arg)
            self.sendqs[ep].append(act)
        elif typ == 'r':
            ep = (arg, proc)
            if ep not in self.recvqs:
                self.recvqs[ep] = []
            act = action(ident, proc, idx, rank=len(self.recvqs[ep]), src=arg, dst=proc)
            self.recvqs[ep].append(act)
        elif typ == 'w':
            req = self[proc][arg - self.filtered[proc]]
            assert req.is_send or req.is_recv
            if self.infinite and req.is_send:
                self.filtered[proc] += 1
                return
            act = action(ident, proc, idx, req=req)
            ep = (req.src, req.dst)
            msgq = self.sendqs[ep] if req.is_send else self.recvqs[ep]
            req_idx = msgq.index(req)
            while msgq[req_idx].wait is None:
                msgq[req_idx].wait = act
                req_idx -= 1
        elif typ == 'b':
            if arg not in self.groups:
                self.groups[arg] = []
            act = action(ident, proc, idx, grp=arg)
            self.groups[arg].append(act)
        self[proc].append(act)

def read(fname):
    """
    :param fname: file containing a CTP
    :returns: a list of source lines with blank and comment lines removed
    """
    source_pat = re.compile('^[srwb]')
    with open(fname, 'r') as log:
        return [l.strip() for l in log.readlines() if source_pat.match(l)]

def parse(lines, add_extra=False, infinite=False):
    """
    :param lines: a list of CTP source lines
    :param add_extra: add final barrier and bottom actions at end of program, default False
    :param infinite: transform to infinite buffer program
    :returns: a CTP as a two dimensional array of actions
    """
    prog = CTP(infinite)
    for ident, line in enumerate(lines):
        parts = line.split()
        assert len(parts) in (3, 4)
        typ = parts[0]
        assert typ in 'srwb'
        proc = int(parts[1])
        arg = int(parts[2])
        prog.append(ident, typ, proc, arg)
    if add_extra:
        prog.add_extra()
    return prog

def serialize(prog):
    """
    :param prog: a CTP as a two dimensional array of actions
    :returns: source lines for prog
    """
    def serialize_act(act):
        if act.is_send:
            return "s " + str(act.src) + " " + str(act.dst) + " " + str(act.idx)
        elif act.is_recv:
            return "r " + str(act.dst) + " " + str(act.src) + " " + str(act.idx)
        elif act.is_wait:
            return "w " + str(act.proc) + " " + str(act.req.idx)
        elif act.is_barr:
            return "b " + str(act.proc) + " " + str(act.grp)
        elif act.is_bot:
            return "e " + str(act.proc)
        return "u"
    return list(map(serialize_act, sorted(flatten_one_level(prog))))

def print_ascii(prog, out=sys.stdout):
    """
    Print prog in table format

    :param prog: a CTP as a two dimensional array of actions
    :param out: a file object to print to, default stdout
    """
    padding = {p: 0 for p in range(len(prog))}
    def pad(proc, s):
        if len(s) >= padding[proc]:
            return s
        else:
            return s + (' ' * (padding[proc] - len(s)))
    for idx, proc in enumerate(prog):
        for act in proc:
            padding[idx] = max([padding[idx], len(str(act))])
    maxidx = max([len(p) for p in prog])
    for idx in range(maxidx):
        parts = []
        for proc, acts in enumerate(prog):
            if idx < len(acts):
                parts.append(pad(proc, str(acts[idx])))
            else:
                parts.append(pad(proc, ''))
        print('|'.join(parts), file=out)
