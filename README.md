# mpivv - MPI Deadlock Verification

Run `python analyze.py -h` to learn how to use the tool.

# Dependencies

- Python 3
- Z3 with Python API

# Overview

The tool takes a formatted log of an MPI program execution as input (see `tests/fixtures/*.ctp`).
It creates a dependency graph over the actions performed during execution to represent potential
communication dependencies in alternate schedules of the same actions.
It detects cycles in this dependency graph (`mpivv/graph.py` and `mpivv/johnson.py`) as potential deadlocks and filters them with an
abstract machine (`mpivv/machine.py`) that implements abstract MPI semantics.
Finally, each surviving deadlock candidate is validated with an SMT encoding (`mpivv/smt.py`).

# Examples

Pretty print program.
```
python analyze.py print tests/fixtures/1.ctp
```

Test if program has a feasible execution and print the execution.
```
python analyze.py feasible tests/fixtures/1.ctp
```

Count cycles and filtered cycles in program.
```
python analyze.py cycles tests/fixtures/1.ctp
```

Some programs don't contain cyles without adding the final barrier and bottom actions and creating a full dependency graph.
```
python analyze.py --extra cycles tests/fixtures/1.ctp
```

Count cycles, filtered cycles and deadlocks in program.
```
python analyze.py --extra deadlocks tests/fixtures/1.ctp
```

Infinite buffer setting.
```
python analyze.py --infinite --extra deadlocks tests/fixtures/1.ctp
```

Don't filter cycles.
```
python analyze.py --no-filter --extra deadlocks tests/fixtures/1.ctp
```

Change log level.
In DEBUG mode the tool pretty prints the deadlock executions as well.
```
python analyze.py --log DEBUG --extra deadlocks tests/fixtures/1.ctp
```

Change limits, default is 1000 cycles and 1 deadlock.
```
python analyze.py --cycle-limit 100 --deadlock-limit 5 --extra deadlocks tests/fixtures/1.ctp
```

